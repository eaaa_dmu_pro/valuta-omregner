﻿using CurrencyConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CurrencyConverter.Controllers
{
    public class CurrencyController : Controller
    {
        // GET: Currency
        public ActionResult Index()
        {
            var model = new CurrencyViewModel();
            model.Currencies = GetCurrency();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CurrencyViewModel model)
        {
            return View("Result", model);
        }

        private List<Currency> GetCurrency()
        {
            return new List<Currency>()
            {
                new Currency
                {
                    Name = "Amerikanske Dollars",
                    Abbreviation = "$",
                    ExchangeRate = 6.54m
                },
                new Currency
                {
                    Name = "Balboa",
                    Abbreviation = "Ba",
                    ExchangeRate = 0.00001m
                },
                new Currency
                {
                    Name = "Euro",
                    Abbreviation = "€",
                    ExchangeRate = 7.45m
                }
            };
        }
    }
}