﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyConverter.Models
{
    public class CurrencyViewModel
    {
        public List<Currency> Currencies { get; set; }
        public decimal Amount { get; set; }
    }
}