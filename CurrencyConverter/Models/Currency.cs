﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyConverter.Models
{
    public class Currency
    {
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool IsSelected { get; set; }

        public decimal Convert(decimal amount)
        {
            return amount / ExchangeRate;
        }
    }
}